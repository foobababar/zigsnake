# zigsnake
snake written in zig using ncurses.

## controls
* h -> go left
* l -> go right
* k -> go up
* j -> go down
* q -> quit


## requirements
ncurses.h (in C)


## how to use
zig build-exe zigsnake.zig -lc -lcurses

or 

zig build && zig build run

## known bugs
* ncurses lag. Too easy to get out of bounds.

## licence 
MIT


## references
credits to :
https://github.com/mnisjk/snake

https://gitlab.com/foobababar/processing-snake-game

https://viewsourcecode.org/snaptoken/kilo/index.html (for window size)

## zig version
0.11.0-dev.48+678f3f6e6
