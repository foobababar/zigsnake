//TODO: ensure game over is working as expected
//TODO: add score maybe ?


////  native imports  \\\\
const std = @import("std");
const print = std.debug.print;
const Allocator = std.mem.Allocator;


////  C imports  \\\\
const c = @cImport({
  @cInclude("ncurses.h");
  @cInclude("termios.h");
  @cInclude("sys/ioctl.h");
  @cInclude("unistd.h"); //for c.STDOUT_FILENO
});


////  data  \\\\
var WIDTH: u16 = undefined;
var HEIGHT: u16 = undefined;
const DEFAULT_WIDTH = 60;
const DEFAULT_HEIGHT = 20;

const MINFOOD = 1;//food x or y won't be smaller than that




var window: ?*c.WINDOW = null;


var frameCount: u32 = 0;
var frameInvul: u32 = 0;
var delayInvul: u16 = 30;



//// errors  \\\\
const err = error {
  getWindowSize,
};


////  structs  \\\\

const Segment = struct {
  x: u16 = undefined,
  y: u16 = undefined,

  pub fn init(self: *Segment) void {
    self.x = 0;
    self.y = 0;
  }

  pub fn display(self: *Segment) void {
    _ = c.mvwaddstr(window, self.y, self.x, "#");    
  }
};


const Snake = struct {
  facing: u8 = 'R',
  mvSpeed: u16 = 10,
  body: ?[]Segment = null, //body is a pointer OR is null


  pub fn addSegment(self: *Snake, allocator: Allocator) !void {
    if(self.body==null){
      self.body = try allocator.alloc(Segment, 1);
      var tmp: *Segment = try allocator.create(Segment);
      tmp.*.init();
      self.body.?[0] = tmp.*;
    }
    else {
      var tmp: []Segment = try allocator.alloc(Segment, self.body.?.len+1);
      for(self.body.?)|_, i|{
        tmp[i] = self.body.?[i];
      }
      var tmp2: *Segment = try allocator.create(Segment);
      //no need to init tmp2. Coordinates will update themselves
      tmp[tmp.len-1] = tmp2.*;
      self.body = tmp;
    }
  }


  pub fn display(self: *Snake) void {
    for(self.body.?)|_, i|{
      self.body.?[i].display();
    }
  }


  pub fn updatePosition(self: *Snake) void {
    var xp: u16 = 0;
    var yp: u16 = 0;
    var xp2: u16 = 0;
    var yp2: u16 = 0;
    
    xp = self.body.?[0].x;
    yp = self.body.?[0].y;


    switch(self.facing){
      'L'=> { 
          if(self.body.?[0].x == 0){
            //x<0 so gameover
            game_over();
          }else{
            self.body.?[0].x-=1;
          }
      },
      'R'=> self.body.?[0].x += 1,
      'U'=> { 
          if(self.body.?[0].y == 0){
            //y<0 so gameover
            game_over();
          }else{
            self.body.?[0].y-=1;
          }
      },
      'D'=> self.body.?[0].y += 1,
      else => {},
    }


    var i: u16 = 1;
    while(i<self.body.?.len):(i+=1){
      if(i%2!=0){
        xp2 = self.body.?[i].x;
        yp2 = self.body.?[i].y;

        self.body.?[i].x = xp;
        self.body.?[i].y = yp;
      }else{

        xp = self.body.?[i].x;
        yp = self.body.?[i].y;

        self.body.?[i].x = xp2;
        self.body.?[i].y = yp2;
      }
    }
  }
};


const Food = struct {
  x: u16 = undefined,
  y: u16 = undefined,
  
  pub fn init(self: *Food, x: u16, y: u16) void {
    self.x = x;
    self.y = y;
  }

  pub fn display(self: *Food) void {
    _ = c.mvwaddstr(window, self.y, self.x, "O");       
  }
};



////  functions  \\\\

//
// Get width & height of active window.
// Returns an array of size 2 with [0]->width ; [1]->height
//

pub fn getWindowSize() ![2]u16 {
  var ws: c.struct_winsize = undefined;
  if(c.ioctl(c.STDOUT_FILENO, c.TIOCGWINSZ, &ws)==1 or ws.ws_col == 0){
    return err.getWindowSize;
  }else{
    return [2]u16{ws.ws_col, ws.ws_row};
  }
}



//
// generate & returns a random integer between min and max
// credits to https://zig.news/sobeston/a-guessing-game-5fb1 for rng 
//

pub fn generateInt(min: u16, max: u16) !u16 {
  var seed: u64 = undefined;
  try std.os.getrandom(std.mem.asBytes(&seed));
  
  var prng = std.rand.DefaultPrng.init(seed);
  const rand = &prng.random();
  return rand.intRangeAtMost(u16, min, max);
}


pub fn game_over() void {
  _ = c.endwin();
  std.os.exit(0);
}


pub fn draw_board() void{
  var x: u16 = 0;
  while(x<HEIGHT):(x+=1){
    _ = c.mvwaddstr(window, x, WIDTH, "|");        
  }

  var y: u16 = 0;
  while(y<WIDTH):(y+=1){
    _ = c.mvwaddstr(window, HEIGHT, y, "-");        
  }
}


pub fn foodIsOnSnake(snake: Snake, x: u16, y: u16) bool {
  for(snake.body.?) |segment|{
    if(segment.x == x and segment.y == y){
      return true;
    }
  }
  return false;
}




pub fn main() !void {
  //allocators & stuff
  var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
  defer arena.deinit();
  const allocator = arena.allocator();
  //No need to free anything. Arena allocator automatically frees everything when arena.deinit();


  // INIT 
  const maxCoordinates = try getWindowSize();

  WIDTH = DEFAULT_WIDTH;
  HEIGHT = DEFAULT_HEIGHT;


  //if window size smaller than default coordinates, set coordinates to window size
  if(maxCoordinates[0]<DEFAULT_WIDTH){
    WIDTH = maxCoordinates[0];
  }

  if(maxCoordinates[1]<DEFAULT_HEIGHT){
    HEIGHT = maxCoordinates[1];
  }


  window = c.initscr();//init ncurses
  _ = c.keypad(window, true);
  _ = c.noecho(); //don't print typed characters
  _ = c.halfdelay(1);
  _ = c.curs_set(0);  //hide cursor
  _ = c.timeout(100);
  


  var snake = Snake{}; 
  try snake.addSegment(allocator);//add 1 segment to be the head of the snake

  var food = Food{};
  food.init(try generateInt(1, WIDTH-2), try generateInt(1, HEIGHT-2));


  while(true){
    const keypressed = c.getch(); //w8 for user input
    if (keypressed != c.ERR){
      switch(keypressed){
        // /!\ snake cannot directly go from facing left to right !
        //                          or up to down !
        'h'=> if(snake.facing!='R') {snake.facing = 'L';},
        'l'=> if(snake.facing!='L') {snake.facing = 'R';},
        'j'=> if(snake.facing!='U') {snake.facing = 'D';},
        'k'=> if(snake.facing!='D') {snake.facing = 'U';},
        'q' => game_over(), //press x to quit
        else => {},
      }
    }

    //clean window 
    _ = c.werase(window);


    //draw stuff on screen
    draw_board();
    snake.display();
    snake.updatePosition();
    food.display();

    //check if snake eats food
    if(snake.body.?[0].x == food.x and snake.body.?[0].y == food.y){
      try snake.addSegment(allocator); 
      frameInvul = frameCount;

      //ensure food doesn't spawn on snake
      var tmpx: u16 = try generateInt(1, WIDTH-2);
      var tmpy: u16 = try generateInt(1, HEIGHT-2);
      while(foodIsOnSnake(snake, tmpx, tmpy)){
        tmpx = try generateInt(MINFOOD, WIDTH-2);
        tmpy = try generateInt(MINFOOD, HEIGHT-2);
      }

      //spawn new food
      food = Food{.x=tmpx, .y=tmpy,};
    }


    //check if snake eats itself
    if(snake.body.?.len>1){
      var index: u16 = 1;
      while(index<snake.body.?.len):(index+=1){
        if( (snake.body.?[0].x == snake.body.?[index].x) and (snake.body.?[0].y == snake.body.?[index].y) and (frameCount > frameInvul + delayInvul) ){
          game_over();
        }
      }
    }


    //check if snake out of bounds
    if(snake.body.?[0].x>WIDTH-1 or snake.body.?[0].y>HEIGHT-1){
      game_over();
    }

    //update frameCount
    frameCount+=1;
  }
  unreachable;
}
